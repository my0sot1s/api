FROM golang:alpine AS builder
# RUN go version
ENV GOPATH /go
ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH

COPY . /go/src/gitlab.com/my0sot1s/api/
COPY . /go/src/gitlab.com/my0sot1s/api/static
WORKDIR /go/src/gitlab.com/my0sot1s/api/

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o app .

# Stage 2 (to create a downsized "container executable", ~7MB)
# If you need SSL certificates for HTTPS, replace `FROM SCRATCH` with:
#
FROM alpine:3.8
#   RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /go/src/gitlab.com/my0sot1s/api/app .
COPY --from=builder /go/src/gitlab.com/my0sot1s/api/static .
# COPY --from=builder /go/src/gitlab.com/my0sot1s/api/.env .

EXPOSE 4041

CMD ["./app","start"]