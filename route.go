package main

import (
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/my0sot1s/model/account"
	"gitlab.com/my0sot1s/model/service"
	"gitlab.com/my0sot1s/model/user"
)

func (r *Router) mapping() {
	router := r.router
	// router.POST("/login", r.signIn)
	// router.POST("/signup", r.signUp)
	// router.GET("/users", r.listUsers)
	// router.GET("/users/id/:id", r.getUser)
	// router.GET("/users/email/:email", r.getUserEmail)
	// router.GET("/users/name/:name", r.getUserName)
	// ------------- account route-------------
	router.GET("/ping", func(ctx *gin.Context) {
		ctx.String(200, "Pong")
	})
	router.POST("/account", r.createAccount)
	router.POST("/account/:account_id", r.updateAccount)
	router.GET("/account/:account_id", r.readAccount)
	// ---------- branch ---------------
	router.POST("/account/:account_id/branches", r.createBranch)
	router.POST("/account/:account_id/branches/:branch_id", r.updateBranch)
	router.GET("/account/:account_id/branches", r.readBranches)
	router.GET("/account/:account_id/branches/:branch_id", r.readBranch)
	router.DELETE("/account/:account_id/branches/:branch_id", r.deleteBranch)
	// ---------- employee ---------------
	router.POST("/account/:account_id/employees", r.createEmployee)
	router.POST("/account/:account_id/employees/:employee_id", r.updateEmployee)
	router.GET("/account/:account_id/employees", r.readEmployees)
	router.GET("/account/:account_id/employees/:employee_id", r.readEmployee)
	router.GET("/account/:account_id/employees/:employeee_id/confirmed", r.confirmedEmployee)
	router.DELETE("/account/:account_id/employees/:employee_id", r.deleteEmployee)
	// ------------------------- service ------------------------------
	router.POST("/service", r.createService)
	router.POST("/service/:service_id/edit", r.updateService)
	router.GET("/service", r.readServices)

	router.POST("/account_service/:account_id/service/:service_id", r.createAccountService)
	router.POST("/account_service/:account_id/service/:service_id/edit", r.updateAccountService)
	router.GET("/account_service/:account_id", r.readAccountService)
	router.DELETE("/account_service/:account_id/:account_service_id", r.deleteAccountService)

}

//---------------authen---------------------

func (r *Router) signIn(ctx *gin.Context) {
	u := &user.User{}
	if err := ctx.BindJSON(&u); err != nil {
		ctx.JSON(400, gin.H{"error": err})
		return
	}
	if token, err := r.client.rpcAuth.SignIn(ctxBground(), &user.User{
		Name:     u.GetName(),
		Password: u.GetPassword(),
	}); err != nil {
		ctx.JSON(400, gin.H{"error": err})
	} else {
		ctx.JSON(200, token)
	}
}
func (r *Router) signUp(ctx *gin.Context) {
	u := &user.User{}
	if err := ctx.BindJSON(&u); err != nil {
		ctx.JSON(400, gin.H{"error": err})
		return
	}
	if uRes, err := r.client.rpcAuth.SignUp(ctxBground(), u); err != nil {
		ctx.JSON(400, gin.H{"error": err})
	} else {
		ctx.JSON(200, uRes)
	}
}
func (r *Router) listUsers(ctx *gin.Context) {
	req := &user.UsersRequest{
		Anchor: ctx.Query("anchor"),
	}
	if ctx.Query("limit") != "" {
		if i, err := strconv.ParseInt(ctx.Query("limit"), 10, 32); err == nil {
			req.Limit = int32(i)
		}
	}
	if usersResp, err := r.client.rpcAuth.ReadUsers(ctxBground(), req); err != nil {
		ctx.JSON(400, gin.H{"error": err})
	} else {
		ctx.JSON(200, usersResp)
	}
}
func (r *Router) getUser(ctx *gin.Context) {
	if resp, err := r.client.rpcAuth.ReadUser(ctxBground(), &user.UsersRequest{
		Id: ctx.Param("id"),
	}); err != nil {
		ctx.JSON(400, gin.H{"error": err})
	} else {
		ctx.JSON(200, resp)
	}
}
func (r *Router) getUserEmail(ctx *gin.Context) {
	resp, err := r.client.rpcAuth.ReadUser(ctxBground(), &user.UsersRequest{
		Email: ctx.Param("email"),
	})
	if err != nil {
		ctx.JSON(400, gin.H{"error": err})
		return
	}
	ctx.JSON(200, resp)
}
func (r *Router) getUserName(ctx *gin.Context) {
	resp, err := r.client.rpcAuth.ReadUser(ctxBground(), &user.UsersRequest{
		Name: ctx.Param("name"),
	})
	if err != nil {
		ctx.JSON(400, gin.H{"error": err})
		return
	}
	ctx.JSON(200, resp)
}

//----------------------account------------------
func (r *Router) createAccount(ctx *gin.Context) {
	acc := &account.Account{}
	if err := ctx.BindJSON(&acc); err != nil {
		ctx.JSON(400, gin.H{"error": err})
		return
	}
	if acc, err := r.client.rpcAccount.CreateAccount(ctxBground(), acc); err != nil {
		ctx.JSON(400, gin.H{"error": err})
	} else {
		ctx.JSON(200, acc)
	}
}
func (r *Router) updateAccount(ctx *gin.Context) {
	acc := &account.Account{}
	if err := ctx.BindJSON(&acc); err != nil {
		ctx.JSON(400, gin.H{"error": err})
		return
	}
	if acc, err := r.client.rpcAccount.UpdateAccount(ctxBground(), acc); err != nil {
		ctx.JSON(400, gin.H{"error": err})
		return
	} else {
		ctx.JSON(200, acc)
	}
}
func (r *Router) readAccount(ctx *gin.Context) {
	req := &account.AccountRequest{
		Id: ctx.Param("account_id"),
	}
	if acc, err := r.client.rpcAccount.ReadAccount(ctxBground(), req); err != nil {
		ctx.JSON(400, gin.H{"error": err})
	} else {
		ctx.JSON(200, acc)
	}
}
func (r *Router) createBranch(ctx *gin.Context) {
	br := &account.Branch{}
	if err := ctx.BindJSON(&br); err != nil {
		ctx.JSON(400, gin.H{"error": err})
		return
	}
	if br, err := r.client.rpcAccount.CreateBranch(ctxBground(), br); err != nil {
		ctx.JSON(400, gin.H{"error": err})
	} else {
		ctx.JSON(200, br)
	}
}
func (r *Router) updateBranch(ctx *gin.Context) {
	br := &account.Branch{}
	if err := ctx.BindJSON(&br); err != nil {
		ctx.JSON(400, gin.H{"error": err})
		return
	}
	if br, err := r.client.rpcAccount.UpdateBranch(ctxBground(), br); err != nil {
		ctx.JSON(400, gin.H{"error": err})
	} else {
		ctx.JSON(200, br)
	}
}
func (r *Router) readBranches(ctx *gin.Context) {
	req := &account.BranchRequest{
		Anchor: ctx.Query("anchor"),
	}
	if ctx.Query("limit") != "" {
		if i, err := strconv.ParseInt(ctx.Query("limit"), 10, 32); err == nil {
			req.Limit = int32(i)
		}
	}
	if branches, err := r.client.rpcAccount.ReadBranches(ctxBground(), req); err != nil {
		ctx.JSON(400, gin.H{"error": err})
	} else {
		ctx.JSON(200, branches)
	}
}
func (r *Router) readBranch(ctx *gin.Context) {
	req := &account.BranchRequest{
		Id:        ctx.Param("branch_id"),
		AccountId: ctx.Param("account_id"),
	}
	if branch, err := r.client.rpcAccount.ReadBranch(ctxBground(), req); err != nil {
		ctx.JSON(400, gin.H{"error": err})
	} else {
		ctx.JSON(200, branch)
	}
}
func (r *Router) deleteBranch(ctx *gin.Context) {
	req := &account.BranchRequest{
		Id:        ctx.Param("branch_id"),
		AccountId: ctx.Param("account_id"),
	}
	if _, err := r.client.rpcAccount.DeleteBranch(ctxBground(), req); err != nil {
		ctx.JSON(400, gin.H{"error": err})
	} else {
		ctx.JSON(200, nil)
	}
}
func (r *Router) createEmployee(ctx *gin.Context) {
	em := &account.Employee{}
	if err := ctx.BindJSON(&em); err != nil {
		ctx.JSON(400, gin.H{"error": err})
		return
	}
	if emResp, err := r.client.rpcAccount.CreateEmployee(ctxBground(), em); err != nil {
		ctx.JSON(400, gin.H{"error": err})
	} else {

		ctx.JSON(200, emResp)
	}
}
func (r *Router) updateEmployee(ctx *gin.Context) {
	em := &account.Employee{}
	if err := ctx.BindJSON(&em); err != nil {
		ctx.JSON(400, gin.H{"error": err})
		return
	}
	if br, err := r.client.rpcAccount.UpdateEmployee(ctxBground(), em); err != nil {
		ctx.JSON(400, gin.H{"error": err})
	} else {
		ctx.JSON(200, br)
	}
}
func (r *Router) readEmployees(ctx *gin.Context) {
	req := &account.EmployeeRequest{
		Anchor: ctx.Query("anchor"),
	}
	if ctx.Query("limit") != "" {
		if i, err := strconv.ParseInt(ctx.Query("limit"), 10, 32); err == nil {
			req.Limit = int32(i)
		}
	}
	if employees, err := r.client.rpcAccount.ReadEmployees(ctxBground(), req); err != nil {
		ctx.JSON(400, gin.H{"error": err})
	} else {
		ctx.JSON(200, employees)
	}
}
func (r *Router) readEmployee(ctx *gin.Context) {
	req := &account.EmployeeRequest{
		Id:        ctx.Param("employee_id"),
		AccountId: ctx.Param("account_id"),
	}
	if em, err := r.client.rpcAccount.ReadEmployee(ctxBground(), req); err != nil {
		ctx.JSON(400, gin.H{"error": err})
	} else {
		ctx.JSON(200, em)
	}
}
func (r *Router) confirmedEmployee(ctx *gin.Context) {
	em := &account.Employee{}
	if err := ctx.BindJSON(&em); err != nil {
		ctx.JSON(400, gin.H{"error": err})
		return
	}
	req := &account.EmployeeRequest{
		Token:    ctx.Query("token"),
		Employee: &account.Employee{Password: em.GetPassword(), Email: em.GetEmail()},
	}
	if employee, err := r.client.rpcAccount.InvitedEmployee(ctxBground(), req); err != nil {
		ctx.JSON(400, gin.H{"error": err})
	} else {
		ctx.JSON(200, employee)
	}
}
func (r *Router) deleteEmployee(ctx *gin.Context) {
	req := &account.EmployeeRequest{
		Id:        ctx.Param("employee_id"),
		AccountId: ctx.Param("account_id"),
	}
	if _, err := r.client.rpcAccount.DeleteEmployee(ctxBground(), req); err != nil {
		ctx.JSON(400, gin.H{"error": err})
	} else {
		ctx.JSON(200, nil)
	}
}

//----------------------service------------------

func (r *Router) createService(ctx *gin.Context) {
	serv := &service.Service{}
	if err := ctx.BindJSON(&serv); err != nil {
		ctx.JSON(400, gin.H{"error": err})
		return
	}
	if serv, err := r.client.rpcService.CreateService(ctxBground(), serv); err != nil {
		ctx.JSON(400, gin.H{"error": err})
	} else {
		ctx.JSON(200, serv)
	}
}
func (r *Router) updateService(ctx *gin.Context) {
	serv := &service.Service{}
	if err := ctx.BindJSON(&serv); err != nil {
		ctx.JSON(400, gin.H{"error": err})
		return
	}
	if serv, err := r.client.rpcService.UpdateService(ctxBground(), serv); err != nil {
		ctx.JSON(400, gin.H{"error": err})
		return
	} else {
		ctx.JSON(200, serv)
	}
}
func (r *Router) readServices(ctx *gin.Context) {
	req := &service.ServiceRequest{
		Anchor: ctx.Query("anchor"),
	}
	if ctx.Query("limit") != "" {
		if i, err := strconv.ParseInt(ctx.Query("limit"), 10, 32); err == nil {
			req.Limit = int32(i)
		}
	}
	if employees, err := r.client.rpcService.ReadServices(ctxBground(), req); err != nil {
		ctx.JSON(400, gin.H{"error": err})
	} else {
		ctx.JSON(200, employees)
	}
}
func (r *Router) createAccountService(ctx *gin.Context) {
	serv := &service.AccountService{}
	if err := ctx.BindJSON(&serv); err != nil {
		ctx.JSON(400, gin.H{"error": err})
		return
	}
	if serv, err := r.client.rpcService.CreateAccountService(ctxBground(), serv); err != nil {
		ctx.JSON(400, gin.H{"error": err})
	} else {
		ctx.JSON(200, serv)
	}
}
func (r *Router) updateAccountService(ctx *gin.Context) {
	serv := &service.AccountService{}
	if err := ctx.BindJSON(&serv); err != nil {
		ctx.JSON(400, gin.H{"error": err})
		return
	}
	if serv, err := r.client.rpcService.UpdateAccountService(ctxBground(), serv); err != nil {
		ctx.JSON(400, gin.H{"error": err})
		return
	} else {
		ctx.JSON(200, serv)
	}
}
func (r *Router) readAccountService(ctx *gin.Context) {
	req := &service.AccountServiceRequest{
		Anchor: ctx.Query("anchor"),
	}
	if ctx.Query("limit") != "" {
		if i, err := strconv.ParseInt(ctx.Query("limit"), 10, 32); err == nil {
			req.Limit = int32(i)
		}
	}
	if employees, err := r.client.rpcService.ReadAccountServices(ctxBground(), req); err != nil {
		ctx.JSON(400, gin.H{"error": err})
	} else {
		ctx.JSON(200, employees)
	}
}
func (r *Router) deleteAccountService(ctx *gin.Context) {
	// req := &service.AccountServiceRequest{
	// 	Id:        ctx.Param("id"),
	// 	AccountId: ctx.Param("account_id"),
	// }
	// if _, err := r.client.rpcService.(ctxBground(), req); err != nil {
	// 	ctx.JSON(400, gin.H{"error": err})
	// } else {
	// 	ctx.JSON(200, nil)
	// }
}
