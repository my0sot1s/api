package main

import (
	"errors"

	"gitlab.com/my0sot1s/auth/token"
	"gitlab.com/my0sot1s/helper"
)

type tokenValid struct {
	tkHelper *token.TokenHelper
}

func (tkv *tokenValid) tkValidInit(publicPath string) error {
	tkv.tkHelper = &token.TokenHelper{}
	return tkv.tkHelper.ChargePublicKey(publicPath)
}

func (tkv *tokenValid) tokenChecker(tk string) (helper.MS, error) {
	payload, err := tkv.tkHelper.ParseToken(tk)
	if err != nil {
		return nil, err
	}
	var user helper.MS
	err = helper.ConveterM2I(payload["payload"], &user)
	if payload["payload"] == nil || err != nil {
		return nil, errors.New("payload not available")
	}
	return user, nil
}
