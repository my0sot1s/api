package token

import (
	"crypto/rsa"
	"errors"
	"io/ioutil"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/my0sot1s/helper"
)

// TokenHelper create method for working jwt
type TokenHelper struct {
	publ *rsa.PublicKey
	priv *rsa.PrivateKey
}

// ChargePublicKey push publickey to Helper
func (t *TokenHelper) ChargePublicKey(path string) error {
	publPerm, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}
	helper.Log("public", string(publPerm))
	publicKey, err := jwt.ParseRSAPublicKeyFromPEM(publPerm)
	t.publ = publicKey
	return err
}

// ChargePrivatekey is push privatekey to Helper
func (t *TokenHelper) ChargePrivatekey(path string) error {
	privPerm, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}
	helper.Log("private", string(privPerm))
	privateKey, err := jwt.ParseRSAPrivateKeyFromPEM(privPerm)
	t.priv = privateKey
	return err
}

// InitToken is do 2 job add public key and private key
func (t *TokenHelper) InitToken(publicPath, privatePath string) error {
	if err := t.ChargePublicKey(publicPath); err != nil {
		helper.Log(err)
		// return err
	}
	if err := t.ChargePrivatekey(privatePath); err != nil {
		helper.Log(err)
		// return err
	}
	return nil
}

// Generate is create token with privatekey
func (t *TokenHelper) Generate(payload interface{}, duration time.Duration) (string, error) {
	//  make header use algorithm RSA 256
	token := jwt.New(jwt.SigningMethodRS256)
	// make payload
	claims := make(jwt.MapClaims)
	claims["exp"] = time.Now().Add(duration).Unix()
	claims["iat"] = time.Now().Unix()
	claims["payload"] = payload
	token.Claims = claims
	// make token signature
	return token.SignedString(t.priv)
}

// ParseToken is verify token string is valid: input is public key
func (t *TokenHelper) ParseToken(tk string) (helper.M, error) {
	token, err := jwt.Parse(tk, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, errors.New(helper.ConvInterface2String(token.Header["alg"]))
		}
		return t.publ, nil
	})
	switch err.(type) {
	case nil: // no error
		if !token.Valid { // but may still be invalid
			return nil, errors.New("token invalid or token failure.-")
		}
	case *jwt.ValidationError: // something was wrong during the validation
		vErr := err.(*jwt.ValidationError)
		switch vErr.Errors {
		case jwt.ValidationErrorExpired:
			return nil, errors.New("token expired.-")
		default:
			return nil, errors.New("token invalid or token failure.-")
		}
	default: // something else went wrong
		errors.New("token invalid or token failure.-")
	}
	if claims, ok := token.Claims.(jwt.MapClaims); !ok || !token.Valid {
		return nil, errors.New("token can not claim or is invalid")
	} else {
		return claims, nil
	}
}
