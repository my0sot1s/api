package main

import (
	"errors"
	"os"

	"github.com/urfave/cli"
	"gitlab.com/my0sot1s/helper"
)

func injectStreamAuthen(client *Client) error {
	if os.Getenv("AUTH_HOST") == "" {
		helper.ErrLog(errors.New("no env AUTH_HOST"))
		return errors.New("no env AUTH_HOST")
	}
	helper.Log(os.Getenv("AUTH_HOST"))
	return client.initRpcAuth(os.Getenv("AUTH_HOST"))
}

func injectStreamAccount(client *Client) error {
	if os.Getenv("ACCOUNT_HOST") == "" {
		helper.ErrLog(errors.New("no env ACCOUNT_HOST"))
		return errors.New("no env ACCOUNT_HOST")
	}
	helper.Log(os.Getenv("ACCOUNT_HOST"))
	return client.initRpcAccount(os.Getenv("ACCOUNT_HOST"))
}

func injectStreamService(client *Client) error {
	if os.Getenv("SERVICE_HOST") == "" {
		helper.ErrLog(errors.New("no env SERVICE_HOST"))
		return errors.New("no env SERVICE_HOST")
	}
	helper.Log(os.Getenv("SERVICE_HOST"))
	return client.initRpcService(os.Getenv("SERVICE_HOST"))
}

func startApp() error {
	app := cli.NewApp()
	app.Action = func(c *cli.Context) error {
		helper.Log("Wow, Do you know my name??")
		return nil
	}
	app.Commands = []cli.Command{cli.Command{
		Name: "start",
		Action: func(ctx *cli.Context) {
			client, service := &Client{}, &Router{}
			injectStreamAccount(client)
			injectStreamAuthen(client)
			injectStreamService(client)
			helper.Log("+++ SERVE STARTED AT : ", os.Getenv("PORT"))
			service.initServe(os.Getenv("PORT"), client)
		}},
	}
	return app.Run(os.Args)
}

func main() {
	helper.Log(helper.TitleConsole("H e l l o   a p i"))
	if err := startApp(); err != nil {
		panic(err)
	}
}
