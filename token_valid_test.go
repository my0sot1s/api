package main

import (
	"testing"

	"gitlab.com/my0sot1s/helper"
)

func Test_tokenChecker(t *testing.T) {
	tk := &tokenValid{}
	if err := tk.tkValidInit("id_rsa.pub"); err != nil {
		t.Error(err)
		return
	}
	tkstr := `eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTcwNTM4NDgsImlhdCI6MTU1NzA1MjA0OCwicGF5bG9hZCI6eyJ1aWQiOiJuZ3V5ZW5uZ29jbmdhbiJ9fQ.FjNTn17KKFuYuEnBUueUtn40T8bzrb73GIGVKqSxABJ2H_YT9P1AvZUNUfDyOV1cMuYYIyPDmZEu8bDqIYWhc2ZUbgxnG5QslhJprtEk_SE-Lo_AcurnLhXvRMeUWDcFpvnrYfFxLvzjA9mEjWKHAAn9zNDbuftyavWU66QQNY8`
	payload, err := tk.tokenChecker(tkstr)
	helper.Log(payload, err)
}
