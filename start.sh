#!/bin/sh
# ./api start
docker build -t my0sot1s/api . --rm

export PORT=4041

docker run --rm -d \
 --name dev_api \
 -e "PORT=${PORT}" \
 -e "USER_SERVICE_HOST=auth:4041" \
#  -e "UNIT_SERVICE_HOST=dev_coraline:4040" \
 -p "4041:${PORT}" \
 my0sot1s/api
echo "run done on background"