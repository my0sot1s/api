package main

import (
	"context"
	"fmt"

	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"

	"gitlab.com/my0sot1s/model/account"
	"gitlab.com/my0sot1s/model/service"
	"gitlab.com/my0sot1s/model/user"
)

/************** defined defination ************/
var ctxBground = context.Background

// RouterCB callback for router
type RouterCB = func(g *Router)

// Router for api
type Router struct {
	router      *gin.Engine
	whiteListed string
	client      *Client
}

// Client is client of grpc
type Client struct {
	rpcAuth    user.UserStreamClient
	rpcAccount account.AccountRPCClient
	rpcService service.ServiceRPCClient
}

/*************************** init addpter ****************/

// InitRouter start gin
func (r *Router) initServe(port string, client *Client) {
	gin.SetMode(gin.ReleaseMode)
	r.router = gin.New()
	r.router.Use(gin.Recovery(), gin.Logger(), middlewareHeader())
	r.mapping()
	r.client = client
	r.router.Run(fmt.Sprintf(":%v", port))
}

// InitStreamUser start grpc client
func (c *Client) initRpcAuth(target string) error {
	client, err := grpc.Dial(target, grpc.WithInsecure())
	if err != nil {
		return err
	}
	c.rpcAuth = user.NewUserStreamClient(client)
	return nil
}

func (c *Client) initRpcAccount(target string) error {
	client, err := grpc.Dial(target, grpc.WithInsecure())
	if err != nil {
		return err
	}
	c.rpcAccount = account.NewAccountRPCClient(client)
	return nil
}

func (c *Client) initRpcService(target string) error {
	client, err := grpc.Dial(target, grpc.WithInsecure())
	if err != nil {
		return err
	}
	c.rpcService = service.NewServiceRPCClient(client)
	return nil
}

/***************** after run adapter *****************/
var headerDefault = map[string]string{
	"Content-Type":                 "application/json;text/html",
	"Access-Control-Allow-Headers": "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization",
	"Access-Control-Allow-Origin":  "*",
}

func respondWithError(code int, message string, c *gin.Context) {
	resp := map[string]string{"error": message}
	c.JSON(code, resp)
	c.AbortWithStatus(code)
}

func mwAuthentication() gin.HandlerFunc {
	return func(c *gin.Context) {
		// check token
		var token string
		if token = c.GetHeader("x-accestoken"); token == "" {
			token = c.Query("accesstoken")
		}
		if token == "" {
			respondWithError(401, "API token required", c)
			return
		}
		// valid token
		c.Next()
	}
}

func middlewareHeader() gin.HandlerFunc {
	return func(c *gin.Context) {
		for k, v := range headerDefault {
			c.Writer.Header().Set(k, v)
		}
		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(200)
			return
		}
		c.Next()
	}
}

//----------------- handle request ------------------

